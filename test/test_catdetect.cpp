#include "../catdetect.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

using namespace ::testing;
using namespace cv;

GTEST_TEST(detect, giventortie_returnstrue) {
  Mat tortie = imread("/src/data/images/tortie-on-refrigerator.jpg");
  ASSERT_TRUE(detectCat(tortie));
}

TEST(detect, givenperson_returnsfalse) {
  Mat person = imread("/src/data/images/clay-with-tie.jpg");
  ASSERT_FALSE(detectCat(person));
}

TEST(detect, givenlightcat_returnstrue) {
  Mat whitefacedcat = imread("/src/data/images/whitefacedcat.jpg");
  ASSERT_TRUE(detectCat(whitefacedcat));
}
TEST(detect, givenTwoCats_returnsTrue) {
  Mat twowhitefacedcats = imread("/src/data/images/two-cats.jpg");
  ASSERT_TRUE(detectCat(twowhitefacedcats));
}

TEST(detect, givenOrangeCat_returnsTrue) {
  Mat orangecat = imread("/src/data/images/orangecat.jpg");
  ASSERT_TRUE(detectCat(orangecat));
}

TEST(detect, givenWhiteFacedKittens_returnsTrue) {
  Mat whitekittens = imread("/src/data/images/white-faced-kittens.jpg");
  ASSERT_TRUE(detectCat(whitekittens));
}