#include "catdetect.h"
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"

#include <iostream>

using namespace std;
using namespace cv;

CascadeClassifier cat_cascade;
CascadeClassifier face_cascade;

Mat resizeImage(Mat src) {
  int width = src.cols;
  int height = src.rows;

  double wfactor = 416.0 / width;
  double hfactor = 416.0 / height;

  double factor = wfactor < hfactor ? wfactor : hfactor;

  int newwidth = width * factor + 1;
  int newheight = height * factor + 1;

  Mat dst(newheight, newwidth, src.type());

  resize(src, dst, Size(newwidth, newheight));

  return dst;
}

bool detectCat(Mat frame) {

  if (!cat_cascade.load("/src/data/haarcascade_frontalcatface.xml")) {
    cerr << "Could not load cat cascade" << endl;
    return false;
  }

  if (!face_cascade.load("/src/data/haarcascade_frontalface_default.xml")) {
    cerr << "Could not load face cascade" << endl;
    return false;
  }

  Mat smallframe = resizeImage(frame);

  Mat frame_gray;

  cvtColor(smallframe, frame_gray, COLOR_BGR2GRAY);
  equalizeHist(frame_gray, frame_gray);

  std::vector<Rect> cats;
  cat_cascade.detectMultiScale(frame_gray, cats);

  std::vector<Rect> faces;
  face_cascade.detectMultiScale(frame_gray, faces);

  if (!cats.empty()) {
    Scalar catcolor(0, 255, 255);

    for (Rect catframe : cats) {
      rectangle(
          smallframe, Point(catframe.x, catframe.y),
          Point(catframe.x + catframe.width, catframe.y + catframe.height),
          catcolor, 4, LINE_8);
    }
  }

  // Debugging block to identify failures
  if (!faces.empty()) {
    Scalar facecolor(255, 255, 0);
    for (Rect faceframe : faces) {
      rectangle(
          smallframe, Point(faceframe.x, faceframe.y),
          Point(faceframe.x + faceframe.width, faceframe.y + faceframe.height),
          facecolor, 4, LINE_8);
    }
  }

  if (!cats.empty())
    imwrite("foundcat.jpg", smallframe);

  return !cats.empty();
}