#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"

#include <cstdlib>
#include <iostream>

using namespace std;
using namespace cv;

void detect(Mat frame);

CascadeClassifier cat_cascade;

int main(int argc, const char **argv) {
  CommandLineParser parser(
      argc, argv,
      "{help h||}"
      "{cat_cascade|data/haarcascades/"
      "haarcascade_frontalcatface.xml|Path to face cascade.}");

  parser.about("\nIdentify cats and follow them.\n\n");
  parser.printMessage();

  String cat_cascade_name = parser.get<String>("cat_cascade");

  if (!cat_cascade.load(cat_cascade_name)) {
    cerr << "--Error loading cat cascade" << endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}