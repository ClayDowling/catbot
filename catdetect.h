#ifndef _CATDETECT_H_
#define _CATDETECT_H_

#include "opencv2/imgproc.hpp"
#include <vector>

using namespace cv;

bool detectCat(Mat frame);

#endif