cmake_minimum_required(VERSION 2.8.12)

project(catbot)

find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})

add_subdirectory(googletest)
add_subdirectory(test)

add_executable(${PROJECT_NAME} catbot.cpp)
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})